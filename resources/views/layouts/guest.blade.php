<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="csrf-token" content="{{ csrf_token() }}">
        <title>Pizza Pazza Due</title>


        <!-- Styles -->
        <link rel="stylesheet" href="{{ asset('css/app.css') }}">

        <meta property="og:image" content="http://yourimage_with_complete_URL.png"/>

        <!-- Scripts -->
        <script src="https://kit.fontawesome.com/1080a31a8f.js" crossorigin="anonymous"></script>
        <script src="{{ mix('js/app.js') }}" defer></script>
    </head>
    <body class="font-sans antialiased" style="background-color: rgb(22,22,22);">
         <div class="container-fluid">
            <div class="row">
                <div class="col-lg-10 ml-auto mr-auto fixed-top">
                    <nav class="navbar navbar-expand-lg navbar-dark shadow-sm p-3 m-0" style="background-color: rgb(42,42,42);">
                        <a class="navbar-brand" href="/">
                            <img class="img-fluid logo ps-2" src="/images/logo_white.png">
                        </a>
                        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarText" aria-controls="navbarText" aria-expanded="false" aria-label="Toggle navigation">
                            <span class="navbar-toggler-icon"></span>
                        </button>
                        <div class="collapse navbar-collapse" id="navbarText">
                            <ul class="navbar-nav ml-auto">
                              <li class="nav-item">
                                <a class="nav-link text-uppercase text-white" href="/downloads/speisekarte.pdf" target="_blank">Speisekarte</a>
                              </li>
                              <li class="nav-item">
                                <a class="nav-link text-uppercase text-white" href="https://shop.pizza-pazzadue.de" target="_blank">Online Bestellen</a>
                              </li>
                              <li class="nav-item">
                                <a class="nav-link text-uppercase text-white" href="tel:02149600903">Tel.: 0214 - 96 00 903</a>
                              </li>
                               <li class="nav-item">
                                <a class="nav-link text-uppercase text-white" href="https://de-de.facebook.com/pages/category/Pizza-Place/Pizza-Pazza-due-950986971646716/" target="_blank">
                                    <i class="fab fa-facebook-square text-white fa-lg"></i>
                                </a>
                              </li>
                               <li class="nav-item">
                                <a class="nav-link text-uppercase text-white" href="https://www.instagram.com/pizzapazzadue/" target="_blank"><i class="fab fa-instagram text-white fa-lg"></i></a>
                              </li>
                              
                            </ul>
                        </div>
                    </nav>
                </div>
            </div>
        </div>
        <main>
            {{ $slot }}
        </main>
        <div class="container-fluid">
            <div class="row">
                <div class="col-lg-10 col-12 ml-auto mr-auto">
                    <footer style="padding: 2.6em; background-color: rgb(42,42,42);">
                        <div class="row py-2">
                            <div class="col-lg-3 py-2">
                                <h3 class="text-primary">Pizza Pazza Due</h3>
                                <p class="text-white">
                                    Bismarckstraße 255<br>
                                    51373 Leverkusen<br>
                                    <a href="tel:02149600903" class="text-white">Tel.: 0214 - 96 00 903</a><br>
                                    <a href="015738103079" class="text-white">Tel.: 0157 - 38 10 30 79</a><br>
                                    <a href="mailto:info@pizza-pazzadue.de" class="text-white">E-Mail: info@pizza-pazzadue.de</a><br>
                                    <a href="https://shop.pizza-pazzadue.de" target="_blank" class="text-white">Shop: shop.pizza-pazzadue.de</a> 
                                </p>
                            </div>
                    
                            <div class="col-lg-3 py-2">
                                <h3 class="text-primary">Öffnungszeiten</h3>
                                <p class="text-white">
                                    Mo. - Do. & So. 11:00 - 22:00 Uhr<br>
                                    Fr. 11:00 - 23:00 Uhr<br>
                                    Sa. 12:00 - 23:00 Uhr<br>
                                    
                                </p>
                                <p class="text-white">
                                    Mindestbestellwert 10€
                                </p>
                            </div>
                            <div class="col-lg-3 py-2">
                                <h3 class="text-primary">Links</h3>
                                <div class="pt-2">
                                    <a class="btn btn-primary text-uppercase" href="/downloads/Speisekarte_2023.pdf" target="_blank">Speisekarte</a>
                                </div>
                                <div class="pt-2">
                                    <a class="btn btn-primary text-uppercase" href="https://shop.pizza-pazzadue.de" target="_blank">Online bestellen</a>
                                </div>
                        
                            </div>
                            <div class="col-lg-3 py-2">
                                <h3 class="text-primary">Rechtliches</h3>
                                <p class="text-white">
                                    <a href="/impressum" class="text-white">Impressum</a><br>
                                    <a href="/datenschutz" class="text-white">Datenschutz</a>
                                </p>
                            </div>
                        </div>
                           
                  
                    </footer>
                </div>
            </div>
        </div>
            
    </body>
</html>