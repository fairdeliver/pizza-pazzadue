<x-guest-layout>

   <!--  <div id = "popup" class="py-5">
        <div class = "container-fluid">
            <div class = "row no-gutters" style="min-height: 100vh;">
                <div class = "col-xl-6 col-lg-8 col-md-10 ml-auto mr-auto align-self-center text-center">
                    <div style="background-color: #000;">
                        <img class = "img-fluid promotionImg" src = "/images/Aktion_PizzaPazzaDue.jpg">
                    <img class = "img-fluid" id="closePopup" src = "/images/closeVar2.png"></div>
                    <div style="background-color: rgb(32,32,32);" class="p-4">
                        <div class="text-center my-2">
                            <a class="text-uppercase bestellen" href="https://shop.pizza-pazzadue.de" target="_blank"><b>Jetzt online bestellen!</b></a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div> -->

    <div class="container-fluid">
        <div class="row">
            <div class="col-lg-10 col-12 ml-auto mr-auto">
                <header>
                    <div class="container-fluid mt-3">
                        <div class="row" style="height:60vh;">
                            <div class="col-xl-9 col-lg-8 ml-auto mr-auto align-self-center">
                                <img class="img-fluid pt-5" src="/images/logo_white.png">
                                <div class="text-center pt-5 mt-3">
                                    <a class="btn btn-primary text-uppercase bg-primary" href="https://shop.pizza-pazzadue.de" target="_blank">Jetzt bestellen</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </header>

                 <div style="background-color: rgb(42,42,42);">
                    <div class="row pt-5 pb-4">
                        <div class="col-lg-10 ml-auto mr-auto text-center align-self-center">
                           <div class="row">
                                <div class="col-lg-4 text-center py-3">
                                    <span class="fa-stack fa-2x ">
                                      <i class="fas fa-circle fa-stack-2x" style="color: #222;"></i>
                                      <i class="fal fa-map-marker-alt fa-stack-1x fa-inverse text-primary"></i>
                                    </span>
                                    <h3 class="h4 text-white pt-3 pb-1">Adresse</h3>
                                     <p class="text-white">
                                        Pizza Pazza Due<br>
                                        Bismarckstraße 255<br>
                                        51373 Leverkusen
                                    </p>
                                </div>
                                  <div class="col-lg-4 text-center py-3">
                                    <span class="fa-stack fa-2x">
                                      <i class="fas fa-circle fa-stack-2x" style="color: #222;"></i>
                                      <i class="fal fa-clock fa-stack-1x fa-inverse text-primary"></i>
                                    </span>
                                    <h3 class="h4 text-white pt-3 pb-1">Öffnungszeiten</h3>
                                    <p class="text-white">
                                        Mo. - Do. & So. 11:00 - 22:00 Uhr<br>
                                        Fr. 11:00 - 23:00 Uhr<br>
                                        Sa. 12:00 - 23:00 Uhr<br>
                                        Wir liefern ab einem Mindestbestellwert von 10€.
                                    </p>
                                </div>
                                  <div class="col-lg-4 text-center py-3">
                                    <span class="fa-stack fa-2x">
                                      <i class="fas fa-circle fa-stack-2x" style="color: #222;"></i>
                                      <i class="fal fa-leaf fa-stack-1x fa-inverse text-primary"></i>
                                    </span>
                                    <h3 class="h4 text-white pt-3 pb-1">Qualität</h3>
                                    <p class="text-white">
                                        Unser Burgerfleisch ist zu 100% aus reinem Rindfleisch. Unsere Gerichte sind in Bio Qualität<br>& wir kochen zu 100% mit pflanzlichem Öl.
                                    </p>
                                </div>
                           </div>
                            
                        </div>
                        <div class="col-lg-4 col-10 col-sm-8 col-md-6 ml-auto mr-auto text-center pb-5 pt-3">
                            <img class="img-fluid py-3" src="/images/paymentIcons.png">
                        </div>
                    </div>
                    
                </div>

                <div style="background-color: rgb(32,32,32);">
                    <div class="row pt-5">
                        <div class="col-lg-10 text-center ml-auto mr-auto">
                            <h1 class="text-uppercase text-primary pt-5 pb-3" style="font-family: 'Lora', serif; font-weight: 300;"><i>Pizza Pazza Due stellt sich vor</i></h1>
                        </div>
                    </div>
                    <div class="row py-5">
                        <div class="col-lg-10 ml-auto mr-auto">
                            <div class="row pt-4">
                                <div class="col-lg-6">
                                    <h3 class="text-primary h4">Über uns</h3>
                                </div>

                            </div>
                            <div class="row pb-4">
                                <div class="col-lg-6">
                                    <p class="pt-2 pb-1 text-white">Eine Pizzeria mit modernen Geist, die alles bietet, wofür die internationale Küche gefeiert wird: traditionelle Gerichte, zubereitet mit hochwertigen Produkten und immer frisch und authentisch.</p>

                                    <p class="py-1 text-white">Essen wie bei Freunden zu Hause! – Das ist unser Motto. Ein Besuch in der Pizza Pazza Due ist wie eine Entdeckungsreise durch die verschiedenen Regionen der Welt.</p>
                                </div>
                                <div class="col-lg-6">

                                    <p class="py-1 text-white">Auf unserer Speisekarte stehen Klassiker wie Pasta & Pizza aber auch Speisen, von denen der Gast wahrscheinlich noch nichts gehört hat.</p>

                                    <p class="pt-1 pb-2 text-white">Ihr findet uns im Herzen Leverkusens auf der Bismarckstraße in der Nähe des Bayer Leverkusen Stadion.</p>
                                </div>
                            </div>

                            <div class="row pb-4 pt-5">
                                <div class="col-4">
                                    <img class="img-fluid" src="/images/img_8.jpg">
                                    
                                </div>
                                <div class="col-4">
                                    <img class="img-fluid" src="/images/img_4.jpg">
                                    
                                </div>
                                <div class="col-4">
                                    <img class="img-fluid" src="/images/img_9.jpg">
                                </div>
                            </div>
                            <div class="row pt-4 pb-5">
                                <div class="col-6">
                                    <img class="img-fluid" src="/images/img_3.jpg">
                                    
                                </div>
                                <div class="col-6">
                                    <img class="img-fluid" src="/images/img_2.jpg">
                                    
                                </div>
                                    
                            </div>
                            <div class="row py-4">
                                <div class="col-lg-6">

                                    <h3 class="text-primary h4">Indische Speisen</h3>

                                    <p class="py-2 text-white">Ganz traditionell bereiten wir indische Gerichte für Sie zu. Mit vielen wertvollen Zutaten werden unsere Hausgemachten indischen Soßen einzigartig zubereitet. Frisches Hähnchen, Lamm und Rindfleisch finden Sie in verschiedenen Variationen auf unsere Karte. </p>


                                    <h3 class="text-primary h4">Italienische Speisen</h3>

                                    <p class="py-2 text-white">Italienisch? Lieben wir doch alle! Knusprige Pizza, cremige Pasta und heiße Aufläufe, dafür brennen nicht nur unsere Öfen, sondern auch unsere Herzen. Unsere Pizza besticht durch einen krossen und gleichzeitig fluffigen Teig. Alles in Handarbeit und mit ganz viel Liebe.</p>

                                    <h3 class="text-primary h4">Asiatisch</h3>

                                    <p class="py-2 text-white">Für die Kunden, die eine Leidenschaft für chinesische Gerichte haben, sind wir natürlich auch vorbereitet. Eine frische und leckere asiatische Küche erwartet Euch ebenfalls bei uns.</p>

                                    <h3 class="text-primary h4">Für den kleinen Hunger</h3>

                                    <p class="py-2 text-white">Heute mal was Leichtes oder Kleines? 
                                    Kein Problem, wir von Pizza Pazza due bieten auch eine Vielzahl an Salaten, Baguetten, sowie diverse kleine Snacks an.</p>
                                </div>
                           
                                <div class="col-lg-6">

                                    <h3 class="text-primary h4">Burger</h3>

                                    <p class="pt-2 pb-4 text-white">Unser Burger Fleisch ist 100 % reines Rind. Liebevoll und frisch bereiten wir unsere Burger in vielen Variationen und Größen für Sie zu. Für unsere Vegetarier ist natürlich auch was dabei.</p>

                                    <h3 class="text-primary h4">Catering</h3>
                                    <p class="pt-2 pb-4 text-white">Ob kleine Partyrunde oder großes Fest – mit dem Partyservice von Pizza pazza due liegen sie richtig.<br>Wir liefern auf Wunsch auch Warmhaltegeräte, Besteck und Geschirr.<br>
                                    Genießen Sie Ihre Party, wir kümmern uns um den Rest. </p>
                                     <a class="btn btn-primary text-uppercase bg-primary" href="/downloads/speisekarte.pdf" target="_blank">Zur Speisekarte</a>
                                </div>
                            </div>
                        </div>
                    </div>
                   
                   
                </div>


                <div class="break">
                    <div class="row" style="min-height: 45vh;">
                        <div class="col-10 ml-auto mr-auto text-center align-self-center">
                            <h2 class="text-uppercase text-white py-4">Jetzt online bestellen<br>& liefern lassen!</h2>

                            <a class="btn text-uppercase text-white" href="https://shop.pizza-pazzadue.de" target="_blank" style="background-color: rgb(42,42,42);">Jetzt bestellen</a>
                            
                        </div>
                    </div>
                </div>

                <div style="background-color: rgb(32,32,32);">
                    <div class="row pt-5">
                        <div class="col-lg-10 text-center ml-auto mr-auto">
                            <h1 class="text-uppercase text-primary pt-5 pb-5"style="font-family: 'Lora', serif; font-weight: 300;"><i>Beliebte Speisen</i></h1>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-lg-4 col-6 py-2">
                            <img class="img-fluid" src="/images/pizza-originale.jpg" alt="Pizza Originale">
                        </div>
                        <div class="col-lg-4 col-6 py-2">
                             
                            <img class="img-fluid" src="/images/mozzarella-salat.jpg" alt="Pizza Originale">
                        </div>
                        <div class="col-lg-4 col-6 py-2">
                            
                            <img class="img-fluid" src="/images/sportler-nudeln.jpg" alt="Pizza Originale">
                        
                        </div>
                        <div class="col-lg-4 col-6 py-2">
                            <img class="img-fluid" src="/images/ente-curry.jpg" alt="Pizza Originale">
                                
                        </div>
                        <div class="col-lg-4 col-6  py-2">
                                <img class="img-fluid" src="/images/steak.jpg" alt="Pizza Originale">
                                
                        </div>
                        <div class="col-lg-4 col-6 py-2">
                                <img class="img-fluid" src="/images/tiramisu.jpg" alt="Pizza Originale">
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-12 text-center align-self-center py-5">

                            <a class="btn btn-primary text-uppercase " href="https://shop.pizza-pazzadue.de" target="_blank">Jetzt bestellen</a>
                            
                        </div>
                    </div>
                </div>
               
            </div>
        </div>

    </div>
</x-guest-layout>
